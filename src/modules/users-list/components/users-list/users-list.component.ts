import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatTableDataSource, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserInterface } from '../../../../interfaces';
import { ApiService } from '../../../core/services';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  displayedColumns = ['first_name', 'last_name', 'email'];
  userList: any[] = [];
  pagesCount: number;
  

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private apiservice: ApiService) {
  }

  ngOnInit() {
   



    this.activatedRoute.data.pipe(
      map(data => data.users)
    )
      .subscribe((users: UserInterface[]) => {
        this.userList = users;
      });

    this.activatedRoute.data.pipe(
      map(data => data.paginationInfo)
    )
      .subscribe(paginationInfo => {
        this.pagesCount = paginationInfo.total;
      })
  }

  pageChanged(event: PageEvent): void {
    
    let page: number = event.pageIndex + 1;
    this.apiservice.fetchUsers(page).subscribe(result => {
      this.userList = result;
      });
      this.router.navigate(['./'], { queryParams: { page } });
  }

  userSelected(user: UserInterface): void {
   
    this.router.navigate(['./user', user.id]);
  }


  
}
